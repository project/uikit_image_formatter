<?php

/**
 * @file
 * Contains uikit_image_formatter.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_help().
 */
function uikit_image_formatter_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the uikit_image_formatter module.
    case 'help.page.uikit_image_formatter':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Lightbox, slideshow and slider formatter for image field') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function uikit_image_formatter_theme() {
  return [
    'uikit_lightbox' => [
      'variables' => [
        'item' => NULL,
        'display_settings' => [],
      ],
      'file' => 'uikit_image_formatter.theme.inc',
    ],
    'uikit_slideshow' => [
      'variables' => [
        'item' => NULL,
        'display_settings' => [],
      ],
      'file' => 'uikit_image_formatter.theme.inc',
    ],
    'uikit_slider' => [
      'variables' => [
        'item' => NULL,
        'display_settings' => [],
      ],
      'file' => 'uikit_image_formatter.theme.inc',
    ],
    'field__uikit_lightbox' => [
      'template' => 'field--uikit-lightbox',
      'base hook' => 'field',
    ],
    'field__uikit_slideshow' => [
      'template' => 'field--uikit-slideshow',
      'base hook' => 'field',
    ],
    'field__uikit_slider' => [
      'template' => 'field--uikit-slider',
      'base hook' => 'field',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function uikit_image_formatter_preprocess_field(&$variables) {
  $element = $variables['element'];
  $target_formatter = ['uikit_lightbox', 'uikit_slideshow', 'uikit_slider'];
  if (in_array($element['#formatter'], $target_formatter)) {
    // Shorten formatter name.
    $formatter = str_replace('uikit_', '', $element['#formatter']);
    if (isset($element['0']['#display_settings'])) {
      // Retrieve settings.
      $settings = $element['0']['#display_settings'];
      // Pass settings to template.
      $variables['settings'] = $settings;
    }
    // Populate main uk-component attribute.
    $variables[$formatter . '_attributes'] = new Attribute();
    $variables[$formatter . '_attributes']['data-uk-' . $formatter] = "";
    foreach ($settings[$formatter] as $setting => $value) {
      $variables[$formatter . '_attributes']['data-uk-' . $formatter] .= $setting . ':' . $value . ';';
    }
    // Collect grid child with classes.
    if (isset($settings['nb-items'])) {
      foreach ($settings['nb-items'] as $nb_item) {
        if (!empty($nb_item)) {
          $variables['grid_child_width_classes'][] = $nb_item;
        }
      }
    }
    if ($formatter == 'slideshow' || $formatter == 'slider') {
      // Create and pass magicall twig variables attributes to template.
      $variables['list_attributes'] = new Attribute(['class' => 'uk-' . $formatter . '-items']);
      $variables['slidenav_prev_attributes'] = new Attribute(
        [
          'data-uk-slidenav-previous' => '',
          'data-uk-' . $formatter . '-item' => 'previous',
        ]);
      $variables['slidenav_next_attributes'] = new Attribute(
        [
          'data-uk-slidenav-next' => '',
          'data-uk-' . $formatter . '-item' => 'next',
        ]);
      // Add caption transition if necessary.
      if ($settings['content']['caption_transition'] != NULL) {
        $transition = ($settings['content']['caption_toggle']) ? '' : 'uk-transition-active';
        $variables[$formatter . '_attributes']['data-uk-' . $formatter] .= "clsActivated: $transition";
      }
      // Build thumbnav images if required.
      if ($settings['navigation']['thumbnav']) {
        // Build thumbnav images.
        $thumbnavs = $variables['items'];
        foreach ($thumbnavs as $key => $value) {
          $thumbnavs[$key]['content']['#display_settings']['content']['style'] = $thumbnavs[$key]['content']['#display_settings']['navigation']['thumbnav-style'];
        }
        $variables['thumbnavs'] = $thumbnavs;
      }
      // Add lightbox capacities.
      if ($settings['lightbox']['enabled']) {
        foreach ($settings['lightbox'] as $setting => $value) {
          if ($setting != 'enabled' && $setting != 'style') {
            $variables['list_attributes']['data-uk-lightbox'] .= $setting . ':' . $value . ';';
          }
        }
      }
    }
    if ($formatter == 'slider') {
      // Populate uk-slider attribute.
      $variables['slider_attributes']['data-uk-slider'] .= 'clsActivated:uk-transition-active;';
      // Build attributes for slider list item.
      if ($settings['content']['gutter']) {
        $variables['list_attributes']['data-uk-grid'] = "";
        $variables['list_attributes']['class'][] = 'uk-grid';
      }
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function uikit_image_formatter_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  $element = $variables['element'];
  $uikit_formatter = ['uikit_lightbox', 'uikit_slideshow', 'uikit_slider'];
  if (in_array($element['#formatter'], $uikit_formatter)) {
    $suggestions[] = 'field__' . $element['#formatter'];
    $suggestions[] = 'field__' . $element['#formatter'] . '__' . $element['#field_type'];
    $suggestions[] = 'field__' . $element['#formatter'] . '__' . $element['#field_name'];
    $suggestions[] = 'field__' . $element['#formatter'] . '__' . $element['#entity_type'] . '__' . $element['#bundle'];
    $suggestions[] = 'field__' . $element['#formatter'] . '__' . $element['#entity_type'] . '__' . $element['#field_name'];
    $suggestions[] = 'field__' . $element['#formatter'] . '__' . $element['#entity_type'] . '__' . $element['#field_name'] . '__' . $element['#bundle'];
  }
}
