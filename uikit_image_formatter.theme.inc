<?php

/**
 * @file
 * UIkit Image Formatter theme preprocess functions.
 */

use Drupal\image\Entity\ImageStyle;
/**
 * Prepares variables for a UIkit lightbox image field formatter.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - display_settings: optional image styles.
 *
 * @ingroup themeable
 */
function template_preprocess_uikit_lightbox(array &$variables) {
  $item = $variables['item'];

  $settings = $variables['display_settings'];

  if($item->getEntity()->bundle() == 'remote_video' || $item->getEntity()->bundle() == 'video') {
    // We have a remote video and need to use thumbnail uri
    $thumbnail = $item->getEntity()->get('thumbnail')->entity;
    $uri = $thumbnail->getFileUri();
  }
  else {
    $uri = $item->entity->getFileUri();
  }



  $alt = !empty($item->alt) ? $item->alt : '';
  $title = !empty($item->title) ? $item->title : '';

  $caption = '';
  if (!empty($alt)) {
    $caption = $alt;
  }
  elseif (!empty($title)) {
    $caption = $title;
  }

  $image = [
    '#theme' => 'image_style',
    '#uri' => $uri,
    '#alt' => $alt,
    '#title' => $title,
    '#attributes' => $item->_attributes,
    '#style_name' => $settings['content']['style'],
  ];

  $dimensions = [];
  if (isset($item->width) && isset($item->height)) {
    $image['#width'] = $dimensions['width'] = $item->width;
    $image['#height'] = $dimensions['height'] = $item->height;
  }

  // Create the path to the image that will show in Lightbox.
  if ($style_name = $settings['content']['lightbox_style']) {
    // Load the image style.
    $style = ImageStyle::load($style_name);
    // Fetch the Image style path from the Image URI.
    $path = $style->buildUrl($uri);
    // Set the dimensions.
    $style->transformDimensions($dimensions, $uri);
  }
  else {
    $path = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
  }

  // Render as a standard image if an image style is not given.
  if (empty($image['#style_name'])) {
    $image['#theme'] = 'image';
  }

  $variables['image'] = $image;

  // if we have a remote video, we need to chnage the path
  if($item->getEntity()->bundle() == 'remote_video') {
    $youtube_video_url = $item->getString();
    $pattern = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
    if (preg_match($pattern, $youtube_video_url, $match)) {
      $variables['yt_id'] = $match[1];
      $variables['path'] = 'https://youtube-nocookie.com/watch?v=' . $match[1];
    }
    else {
      $variables['path'] = $youtube_video_url;
    }
  }
  elseif($item->getEntity()->bundle() == 'video') {
    $video = $item->getEntity()->get('field_media_video_file')->entity;
    $path = \Drupal::service('file_url_generator')->generateString($video->getFileUri());
    $variables['path'] = $path;
  }
  else {
    $variables['path'] = $path;
  }


  if (!empty($caption)) {
    $variables['attributes'] = [
      'data-alt' => $caption,
      'data-caption' => $caption,
    ];
  }
}

/**
 * Prepares variables for a UIkit Slideshow image field formatter.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - display_settings: optional image styles.
 *
 * @ingroup themeable
 */
function template_preprocess_uikit_slideshow(array &$variables) {
  $item = $variables['item'];
  $settings = $variables['display_settings'];
  if (isset($item->entity)) {
    // We have an internal file.
    $uri = $item->entity->getFileUri();
    $mime_type = $item->entity->getMimetype();
    $mime_parts = explode('/', $mime_type);
    if ($mime_parts[0] == 'image') {
      // We have an image.
      $alt = !empty($item->alt) ? $item->alt : '';
      $title = !empty($item->title) ? $item->title : '';

      $caption = '';
      if (!empty($alt)) {
        $caption = $alt;
      }
      elseif (!empty($title)) {
        $caption = $title;
      }

      $image = [
        '#theme' => 'image_style',
        '#uri' => $uri,
        '#alt' => $alt,
        '#title' => $title,
        '#attributes' => ['class' => 'uk-cover'],
        '#style_name' => $settings['content']['style'],
      ];

      $dimensions = [];
      if (isset($item->width) && isset($item->height)) {
        $image['#width'] = $dimensions['width'] = $item->width;
        $image['#height'] = $dimensions['height'] = $item->height;
      }

      // Render as a standard image if an image style is not given.
      if (empty($image['#style_name'])) {
        $image['#theme'] = 'image';
      }

      // Create the path to the image for Lightbox if enabled.
      if ($settings['lightbox']['enabled']) {
        if ($style_name = $settings['lightbox']['style']) {
          // Load the image style.
          $style = ImageStyle::load($style_name);
          // Fetch the Image style path from the Image URI.
          $path = $style->buildUrl($uri);
          // Set the dimensions.
          $style->transformDimensions($dimensions, $uri);
        }
        else {
          $path = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
        }
        if (!empty($caption)) {
          $variables['attributes'] = [
            'data-alt' => $caption,
            'data-caption' => $caption,
          ];
        }
        $variables['path'] = $path;
      }

      $variables['uri'] = $uri;
      $variables['image'] = $image;
    }
    if ($mime_parts[0] == 'video') {
      // We have a video.
      $variables['video'] = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
    }
  }
  else {
    $youtube_video_url = $item->getString();
    $pattern = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
    if (preg_match($pattern, $youtube_video_url, $match)) {
      $variables['remote'] = $match[1];
    }
    else {
      $variables['remote'] = $youtube_video_url;
    }
  }
}

/**
 * Prepares variables for a UIkit Slider image field formatter.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - display_settings: optional image styles.
 *
 * @ingroup themeable
 */
function template_preprocess_uikit_slider(array &$variables) {
  $item = $variables['item'];
  $settings = $variables['display_settings'];
  $uri = $item->entity->getFileUri();
  $alt = !empty($item->alt) ? $item->alt : '';
  $title = !empty($item->title) ? $item->title : '';

  $caption = '';
  if (!empty($alt)) {
    $caption = $alt;
  }
  elseif (!empty($title)) {
    $caption = $title;
  }

  $image = [
    '#theme' => 'image_style',
    '#uri' => $uri,
    '#alt' => $alt,
    '#title' => $title,
    '#attributes' => $item->_attributes,
    '#style_name' => $settings['content']['style'],
  ];

  $dimensions = [];
  if (isset($item->width) && isset($item->height)) {
    $image['#width'] = $dimensions['width'] = $item->width;
    $image['#height'] = $dimensions['height'] = $item->height;
  }

  // Render as a standard image if an image style is not given.
  if (empty($image['#style_name'])) {
    $image['#theme'] = 'image';
  }

  // Create the path to the image for Lightbox if enabled.
  if ($settings['lightbox']['enabled']) {
    if ($style_name = $settings['lightbox']['style']) {
      // Load the image style.
      $style = ImageStyle::load($style_name);
      // Fetch the Image style path from the Image URI.
      $path = $style->buildUrl($uri);
      // Set the dimensions.
      $style->transformDimensions($dimensions, $uri);
    }
    else {
      $path = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
    }
    if (!empty($caption)) {
      $variables['attributes'] = [
        'data-alt' => $caption,
        'data-caption' => $caption,
      ];
    }
    $variables['path'] = $path;
  }

  $variables['uri'] = $uri;
  $variables['image'] = $image;
}
