INTRODUCTION
------------

This module brings 3 new formatter for Image Field, based on UIkit 3 library :
* Lightbox (gallery) --> https://getuikit.com/docs/lightbox
* Slideshow --> https://getuikit.com/docs/slideshow
* Slider (carousel) --> https://getuikit.com/docs/slider

REQUIREMENTS
------------

This module requires the <a href="https://www.drupal.org/project/uikit">
UIkit Drupal Base Theme</a> (or that you load the UIkit 3 library by yourself).

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

CONFIGURATION
-------------

After enable the module, just visit the "Manage display" section for an entity
where you have an image field.
you'll find 3 new formatters : "UIkit Lightbox", "UIkit Slideshow" and 
"UIkit - Slider" to configure. Choose the one you need.

If you want use these formatters when you work with field in views,
you have to enable the views option "enable field template" under
the styling tab.

CUSTOMISATION
-------------

If you need to adjust the component to your needs, simply override
"field--uikit-lightbox.html.twig", "field--uikit-slideshow.html.twig" or"
field--uikit-slider.html.twig". Most interesting classes and attributes are
available in the field template, using TWIG logic attributes.
