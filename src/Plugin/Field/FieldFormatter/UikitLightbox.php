<?php

namespace Drupal\uikit_image_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'uikit_lightbox' formatter.
 *
 * @FieldFormatter(
 *   id = "uikit_lightbox",
 *   label = @Translation("Uikit lightbox"),
 *   field_types = {
 *     "image",
 *     "entity_reference"
 *   }
 * )
 */
class UikitLightbox extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'content' => [
        'style' => '',
        'lightbox_style' => '',
        'legend' => TRUE,
      ],
      'nb-items' => [
        'width_' => 1,
        'width_@s' => 2,
        'width_@m' => 3,
        'width_@l' => 4,
        'width_@xl' => 4,
      ],
      'lightbox' => [
        'animation' => 'slide',
        'autoplay' => TRUE,
        'autoplay-interval' => 5000,
        'pause-on-hover' => TRUE,
      ],
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $image_styles = image_style_options(FALSE);

    $element['content'] = [
      '#title' => $this->t('Content display'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('content')['style'],
        $this->getSetting('content')['lightbox_style'],
        $this->getSetting('content')['legend'],
      ],
    ];

    $element['content']['style'] = [
      '#title' => $this->t('Image style for content'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('content')['style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['content']['lightbox_style'] = [
      '#title' => $this->t('Image style for lightbox'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('content')['lightbox_style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['content']['legend'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display alt text as caption in content'),
      '#default_value' => $this->getSetting('content')['legend'],
    ];

    // Number element.
    $element['nb-items'] = [
      '#title' => $this->t('Number element'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('nb-items')['width_'],
        $this->getSetting('nb-items')['width_@s'],
        $this->getSetting('nb-items')['width_@m'],
        $this->getSetting('nb-items')['width_@l'],
        $this->getSetting('nb-items')['width_@xl'],
      ],
    ];

    $args = [
      '@href' => 'https://getuikit.com/docs/grid',
      '@title' => 'Grid component - UIkit documentation',
    ];

    $breakpoints = [
      '' => $this->t('Affects all device widths'),
      '@s' => $this->t('Affects device widths of 640px and higher.'),
      '@m' => $this->t('Affects device widths of 960px and higher.'),
      '@l' => $this->t('Affects device widths of 1200px and higher.'),
      '@xl' => $this->t('Affects device widths of 1600px and higher.'),
    ];

    $element['nb-items']['grid_columns'] = [
      '#type' => 'item',
      '#title' => $this->t('Grid columns'),
      '#description' => $this->t("To create a grid whose child elements' widths are evenly split, we apply one class to the grid for each breakpoint. Each item in the grid is then automatically applied a width based on the number of columns selected for each breakpoint. See <a href='@href' target='_blank' title='@title'>Grid component</a> for more details.", $args),
    ];

    foreach (['', '@s', '@m', '@l', '@xl'] as $size) {
      $element['nb-items']["width_{$size}"] = [
        '#type' => 'select',
        '#title' => $this->t('uk-child-width-*') . $size,
        '#required' => FALSE,
        '#default_value' => $this->getSetting('nb-items')["width_{$size}"],
        '#options' => [
          "" => $this->t('Not Use'),
          "uk-child-width-1-1{$size}" => 1,
          "uk-child-width-1-2{$size}" => 2,
          "uk-child-width-1-3{$size}" => 3,
          "uk-child-width-1-4{$size}" => 4,
          "uk-child-width-1-5{$size}" => 5,
          "uk-child-width-1-6{$size}" => 6,
        ],
        '#description' => $breakpoints[$size],
      ];
    }

    $element['lightbox'] = [
      '#title' => $this->t('Lightbox display'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('content')['style'],
        $this->getSetting('content')['legend'],
      ],
    ];

    $animation_effects = [
      'slide' => $this->t('slide'),
      'fade' => $this->t('fade'),
      'scale' => $this->t('scale'),
    ];

    $element['lightbox']['animation'] = [
      '#title' => $this->t('Lightbox animation mode'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('lightbox')['animation'],
      '#options' => $animation_effects,
    ];

    $element['lightbox']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lightbox autoplays'),
      '#default_value' => $this->getSetting('lightbox')['autoplay'],
    ];

    $element['lightbox']['autoplay-interval'] = [
      '#type' => 'number',
      '#title' => $this->t('The delay between switching slides in autoplay mode.'),
      '#default_value' => $this->getSetting('lightbox')['autoplay-interval'],
      '#min' => 3000,
      '#step' => 1000,
    ];

    $element['lightbox']['pause-on-hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause autoplay mode on hover.'),
      '#default_value' => $this->getSetting('lightbox')['pause-on-hover'],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];
    // Implement settings summary.
    $image_styles = image_style_options(FALSE);
    // Content Image style.
    if (isset($image_styles[$this->getSetting('content')['style']])) {
      $summary[] = $this->t('Content image style: @style', ['@style' => $image_styles[$this->getSetting('content')['style']]]);
    }
    else {
      $summary[] = $this->t('Content image style: Original image');
    }
    // Display legend in content.
    $summary[] = $this->t('Display alt text as caption in content: @display_legend', ['@display_legend' => $this->getSetting('content')['legend'] ? 'yes' : 'no']);
    // Image style in lightbox.
    if (isset($image_styles[$this->getSetting('lightbox_style')])) {
      $summary[] = $this->t('Lightbox image style: @style', ['@style' => $image_styles[$this->getSetting('lightbox')['style']]]);
    }
    else {
      $summary[] = $this->t('Lightbox image style: Original image');
    }
    // Lightbox animation effect.
    $summary[] = $this->t('Lightbox animation mode: @animation', ['@animation' => $this->getSetting('lightbox')['animation']]);
    // Lightbox autoplay.
    $summary[] = $this->t('Lightbox autoplays: @autoplay', ['@autoplay' => $this->getSetting('lightbox')['autoplay'] ? 'yes' : 'no']);
    // Lightbox Autoplay interval.
    $summary[] = $this->t('Autoplay interval: @autoplay-interval (in milliseconds)', ['@autoplay-interval' => $this->getSetting('lightbox')['autoplay-interval']]);
    // Lightbox Pause on hover.
    $summary[] = $this->t('Pause on hover: @pause-on-hover', ['@pause-on-hover' => $this->getSetting('lightbox')['pause-on-hover'] ? 'yes' : 'no']);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    // Case field item is a media, we need to get the media source fields.
    if ($items->getItemDefinition()->getDataType() == "field_item:entity_reference") {
      $m_items = [];
      foreach ($items as $delta => $item) {
        // Magical.
        $media = $item->entity;
        $source_field_name = $media->getSource()->getConfiguration()['source_field'];
        $m_items[] = $media->get($source_field_name);
      }
      $items = $m_items;
    }

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'uikit_lightbox',
        '#item' => $item,
        '#display_settings' => $settings,
      ];
    }
    return $elements;
  }

}
