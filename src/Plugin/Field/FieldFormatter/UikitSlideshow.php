<?php

namespace Drupal\uikit_image_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'uikit_slideshow' formatter.
 *
 * @FieldFormatter(
 *   id = "uikit_slideshow",
 *   label = @Translation("Uikit slideshow"),
 *   field_types = {
 *     "image",
 *     "entity_reference"
 *   }
 * )
 */
class UikitSlideshow extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'content' => [
        'style' => '',
        'legend' => TRUE,
        'caption_background' => NULL,
        'caption_transition' => NULL,
        'caption_position' => NULL,
        'caption_modifier' => NULL,
        'caption_toggle' => FALSE,
      ],
      'slideshow' => [
        'animation' => 'slide',
        'autoplay' => TRUE,
        'autoplay-interval' => '6000',
        'pause-on-hover' => TRUE,
        'finite' => FALSE,
        'min-height' => '',
        'max-height' => '',
        'ratio' => '16:9',
        'velocity' => 1,
      ],
      'navigation' => [
        'slidenav' => TRUE,
        'slidenav_outside' => FALSE,
        'slidenav_big' => FALSE,
        'dotnav' => FALSE,
        'thumbnav' => FALSE,
        'thumbnav-style' => 'thumbnail',
        'light' => FALSE,
      ],
      'lightbox' => [
        'enabled' => FALSE,
        'style' => '',
        'animation' => 'slide',
        'autoplay' => TRUE,
        'autoplay-interval' => 5000,
        'pause-on-hover' => TRUE,
      ],
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $image_styles = image_style_options(TRUE);

    $element['content'] = [
      '#title' => $this->t('Content display'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('content')['style'],
        $this->getSetting('content')['legend'],
      ],
    ];

    $element['content']['style'] = [
      '#title' => $this->t('Image style for content'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('content')['style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['content']['legend'] = [
      '#title' => $this->t('Display legend in overlay'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('content')['legend'],
    ];

    $element['content']['caption_background'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay background for legend'),
      '#empty_option' => $this->t('-None-'),
      '#options' => [
        'uk-overlay-default' => $this->t('default'),
        'uk-overlay-primary' => $this->t('Primary'),
        'uk-overlay-secondary' => $this->t('Secondary'),
      ],
      '#default_value' => $this->getSetting('content')['caption_background'],
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption transition'),
      '#empty_option' => $this->t('-None-'),
      '#default_value' => $this->getSetting('content')['caption_transition'],
      '#options' => [
        'uk-transition-fade' => $this->t('fade'),
        'uk-transition-slide-top' => $this->t('slide from the top'),
        'uk-transition-slide-bottom' => $this->t('slide from the bottom'),
        'uk-transition-slide-left' => $this->t('slide from the left'),
        'uk-transition-slide-right' => $this->t('slide from the right'),
        'uk-transition-slide-top-small' => $this->t('slide from the top with a smaller distance'),
        'uk-transition-slide-bottom-small' => $this->t('slide from the bottom with a smaller distance'),
        'uk-transition-slide-left-small' => $this->t('slide from the left with a smaller distance'),
        'uk-transition-slide-right-small' => $this->t('slide from the right with a smaller distance'),
        'uk-transition-slide-top-medium' => $this->t('slide from the top  with a medium distance'),
        'uk-transition-slide-bottom-medium' => $this->t('slide from the bottom  with a medium distance'),
        'uk-transition-slide-left-medium' => $this->t('slide from the left  with a medium distance'),
        'uk-transition-slide-right-medium' => $this->t('slide from the right  with a medium distance'),
      ],
      '#description' => $this->t('Transition effect for the caption'),
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption position'),
      '#default_value' => $this->getSetting('content')['caption_position'],
      '#options' => [
        'uk-position-top' => $this->t('Position element at the top'),
        'uk-position-top-left' => $this->t('Position element at the top left'),
        'uk-position-top-center' => $this->t('Position element at the top center'),
        'uk-position-top-right' => $this->t('Position element at the top right'),
        'uk-position-center' => $this->t('Position element vertically centered in the middle'),
        'uk-position-center-left' => $this->t('Position element vertically centered on the left'),
        'uk-position-center-right' => $this->t('Position element vertically centered on the right'),
        'uk-position-bottom' => $this->t('Position element at the bottom'),
        'uk-position-bottom-left' => $this->t('Position element at the bottom left'),
        'uk-position-bottom-center' => $this->t('Position element at the bottom center'),
        'uk-position-bottom-right' => $this->t('Position element at the bottom right'),
        'uk-position-left' => $this->t('Position element at the left'),
        'uk-position-right' => $this->t('Position element at the right'),
        'uk-position-cover' => $this->t('Position element to cover its container'),
      ],
      '#description' => $this->t('Position for the caption'),
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_modifier'] = [
      '#type' => 'select',
      '#title' => $this->t("Caption margin (modifier)"),
      '#empty_option' => $this->t('-None-'),
      '#default_value' => $this->getSetting('content')['caption_modifier'],
      '#options' => [
        'uk-position-small' => $this->t('Small'),
        'uk-position-medium' => $this->t('Medium'),
        'uk-position-large' => $this->t('Large'),
      ],
      '#description' => $this->t('Apply a margin to positioned caption'),
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_toggle'] = [
      '#title' => $this->t('Display Legend only on hover'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('content')['caption_toggle'],
      '#fieldset' => 'slider_options',
    ];

    $element['slideshow'] = [
      '#title' => $this->t('Slideshow settings'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('slideshow')['animation'],
        $this->getSetting('slideshow')['autoplay'],
        $this->getSetting('slideshow')['autoplay-interval'],
        $this->getSetting('slideshow')['pause-on-hover'],
        $this->getSetting('slideshow')['finite'],
        $this->getSetting('slideshow')['min-height'],
        $this->getSetting('slideshow')['max-height'],
        $this->getSetting('slideshow')['ratio'],
        $this->getSetting('slideshow')['velocity'],
      ],
    ];

    $animation_effects = [
      'slide' => $this->t('slide'),
      'fade' => $this->t('fade'),
      'scale' => $this->t('scale'),
      'pull' => $this->t('pull'),
      'push' => $this->t('push'),
    ];
    $element['slideshow']['animation'] = [
      '#title' => $this->t('Slideshow animation mode'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('slideshow')['animation'],
      '#options' => $animation_effects,
    ];

    $element['slideshow']['autoplay'] = [
      '#title' => $this->t('Slideshow autoplays'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slideshow')['autoplay'],
    ];

    $element['slideshow']['autoplay-interval'] = [
      '#title' => $this->t('The delay between switching slides in autoplay mode'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('slideshow')['autoplay-interval'],
      '#min' => 3000,
      '#step' => 1000,
    ];

    $element['slideshow']['pause-on-hover'] = [
      '#title' => $this->t('Pause autoplay mode on hover'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slideshow')['pause-on-hover'],
    ];

    $element['slideshow']['finite'] = [
      '#title' => $this->t('Disable infinite sliding'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slideshow')['finite'],
    ];

    $element['slideshow']['min-height'] = [
      '#title' => $this->t('A minimum height for slideshow (px)'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('slideshow')['min-height'],
    ];

    $element['slideshow']['max-height'] = [
      '#title' => $this->t('A maximum height for slideshow (px)'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('slideshow')['max-height'],
    ];

    $element['slideshow']['ratio'] = [
      '#title' => $this->t('Image ratio in slideshow'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('slideshow')['ratio'],
      '#description' => $this->t('With "The slideshow always takes up the full width of its parent container. The height adapts according to the defined ratio. To change the default ratio of 16:9, just add the ratio option to the attribute. It\'s recommended to use the same ratio as the background images. For example, just use their width and height, like 1600:1200.'),
    ];

    $element['slideshow']['velocity'] = [
      '#title' => $this->t('The animation velocity (pixel/ms)'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('slideshow')['velocity'],
      '#min' => 1,
      '#step' => 1,
    ];

    // Navigation and Overlay.
    $element['navigation'] = [
      '#title' => $this->t('Navigation'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('navigation')['slidenav'],
        $this->getSetting('navigation')['dotnav'],
        $this->getSetting('navigation')['thumbnav'],
        $this->getSetting('navigation')['thumbnav-style'],
      ],
    ];

    $element['navigation']['slidenav'] = [
      '#title' => $this->t('Slidenav'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['slidenav'],
    ];

    $element['navigation']['slidenav_outside'] = [
      '#title' => $this->t('Display slidenav buttons outside slider'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['slidenav_outside'],
      '#fieldset' => 'slider_options',
    ];

    $element['navigation']['slidenav_big'] = [
      '#title' => $this->t('increase the size of the slidenav icons'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['slidenav_big'],
      '#fieldset' => 'slider_options',
    ];

    $element['navigation']['dotnav'] = [
      '#title' => $this->t('Dotnav'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['dotnav'],
    ];

    $element['navigation']['thumbnav'] = [
      '#title' => $this->t('Thumbnav'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['thumbnav'],
    ];

    $element['navigation']['thumbnav-style'] = [
      '#title' => $this->t('Image style for thumbnav'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('navigation')['thumbnav-style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['navigation']['light'] = [
      '#title' => $this->t('Inverse component'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['light'],
      '#description' => $this->t('Add the .uk-light class to improve the visibility of objects on dark backgrounds in a light style.'),
    ];

    $element['lightbox'] = [
      '#title' => $this->t('Lightbox display'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('lightbox')['enabled'],
        $this->getSetting('lightbox')['style'],
        $this->getSetting('lightbox')['animation'],
        $this->getSetting('lightbox')['autoplay'],
        $this->getSetting('lightbox')['autoplay-interval'],
        $this->getSetting('lightbox')['pause-on-hover'],
      ],
    ];

    $element['lightbox']['enabled'] = [
      '#title' => $this->t('Enable lightbox'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('lightbox')['enabled'],
    ];

    $element['lightbox']['style'] = [
      '#title' => $this->t('Image style for content'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('lightbox')['style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $animation_effects = [
      'slide' => $this->t('slide'),
      'fade' => $this->t('fade'),
      'scale' => $this->t('scale'),
    ];

    $element['lightbox']['animation'] = [
      '#title' => $this->t('Lightbox animation mode'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('lightbox')['animation'],
      '#options' => $animation_effects,
    ];

    $element['lightbox']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lightbox autoplays'),
      '#default_value' => $this->getSetting('lightbox')['autoplay'],
    ];

    $element['lightbox']['autoplay-interval'] = [
      '#type' => 'number',
      '#title' => $this->t('The delay between switching slides in autoplay mode.'),
      '#default_value' => $this->getSetting('lightbox')['autoplay-interval'],
      '#min' => 3000,
      '#step' => 1000,
    ];

    $element['lightbox']['pause-on-hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause autoplay mode on hover.'),
      '#default_value' => $this->getSetting('lightbox')['pause-on-hover'],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    // Implement settings summary.
    $summary = [];
    // Image style in slideshow.
    $image_styles = image_style_options(FALSE);
    if (isset($image_styles[$this->getSetting('content')['style']])) {
      $summary[] = $this->t('Content image style: @style', ['@style' => $image_styles[$this->getSetting('content')['style']]]);
    }
    else {
      $summary[] = $this->t('Content image style: Original image');
    }
    // Display legend.
    if ($this->getSetting('content')['legend']) {
      $summary[] = $this->t('Display legend in overlay: yes');
    }
    // Slideshow animation.
    $summary[] = $this->t('Slideshow animation mode: @animation', ['@animation' => $this->getSetting('slideshow')['animation']]);
    // Slideshow autoplay.
    $summary[] = $this->t('Slideshow autoplays: @autoplay', ['@autoplay' => $this->getSetting('slideshow')['autoplay'] ? 'yes' : 'no']);
    // Slideshow autoplay interval.
    $summary[] = $this->t('Autoplay interval: @autoplay-interval (in milliseconds)', ['@autoplay-interval' => $this->getSetting('slideshow')['autoplay-interval']]);
    // Slideshow pause on hover.
    $summary[] = $this->t('Pause on hover: @pause-on-hover', ['@pause-on-hover' => $this->getSetting('slideshow')['pause-on-hover'] ? 'yes' : 'no']);
    // Slideshow infinite loop.
    $summary[] = $this->t('Disable infinite sliding: @finite', ['@finite' => $this->getSetting('slideshow')['finite'] ? 'yes' : 'no']);
    // Slideshow min height.
    if (!empty($this->getSetting('min-height'))) {
      $summary[] = $this->t('Min height slideshow: @min-height', ['@min-height' => $this->getSetting('slideshow')['min-height']]);
    }
    // Slideshow max height.
    if (!empty($this->getSetting('max-height'))) {
      $summary[] = $this->t('Max height slideshow: @max-height', ['@max-height' => $this->getSetting('slideshow')['max-height']]);
    }
    // Slideshow ratio.
    $summary[] = $this->t('Ratio: @ratio', ['@ratio' => $this->getSetting('slideshow')['ratio']]);
    // Slideshow animation velocity.
    $summary[] = $this->t('The animation velocity (pixel/ms).: @velocity', ['@velocity' => $this->getSetting('slideshow')['velocity']]);
    // Slidenav.
    if ($this->getSetting('navigation')['slidenav']) {
      $summary[] = $this->t('Slidenav: yes');
    }
    // Dotnav.
    if ($this->getSetting('navigation')['dotnav']) {
      $summary[] = $this->t('Dotnav: yes');
    }
    // Thumbnav.
    if ($this->getSetting('navigation')['thumbnav']) {
      $summary[] = $this->t('Thumbnav: yes');
    }
    // Lightbox enabled.
    if ($this->getSetting('lightbox')['enabled']) {
      $summary[] = $this->t('Lightbox: yes');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    ;
    $elements = [];
    $settings = $this->getSettings();

    // Case field item is a media, we need to get the media source fields.
    if ($items->getItemDefinition()->getDataType() == "field_item:entity_reference") {
      $m_items = [];
      foreach ($items as $delta => $item) {
        // Magical.
        $media = $item->entity;
        $source_field_name = $media->getSource()->getConfiguration()['source_field'];
        $m_items[] = $media->get($source_field_name);
      }
      $items = $m_items;
    }

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'uikit_slideshow',
        '#item' => $item,
        '#display_settings' => $settings,
      ];
    }

    return $elements;
  }

}
