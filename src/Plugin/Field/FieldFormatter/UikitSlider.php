<?php

namespace Drupal\uikit_image_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'uikit_slider' formatter.
 *
 * @FieldFormatter(
 *   id = "uikit_slider",
 *   label = @Translation("Uikit slider"),
 *   field_types = {
 *     "image",
 *     "entity_reference"
 *   }
 * )
 */
class UikitSlider extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'content' => [
        'style' => '',
        'gutter' => FALSE,
        'legend' => TRUE,
        'caption_backgound' => NULL,
        'caption_transition' => NULL,
        'caption_position' => NULL,
        'caption_modifier' => NULL,
        'caption_toggle' => FALSE,
      ],
      'slider' => [
        'autoplay' => TRUE,
        'autoplay-interval' => '7000',
        'center' => FALSE,
        'finite' => FALSE,
        'pause-on-hover' => TRUE,
        'sets' => FALSE,
        'velocity' => 1,
      ],
      'nb-items' => [
        'width_' => 1,
        'width_@s' => 2,
        'width_@m' => 3,
        'width_@l' => 4,
        'width_@xl' => 4,
      ],
      'navigation' => [
        'slidenav' => TRUE,
        'slidenav_outside' => FALSE,
        'slidenav_big' => FALSE,
        'dotnav' => FALSE,
        'thumbnav' => FALSE,
        'thumbnav-style' => 'thumbnail',
        'light' => FALSE,
      ],
      'lightbox' => [
        'enabled' => FALSE,
        'style' => '',
        'animation' => 'slide',
        'autoplay' => TRUE,
        'autoplay-interval' => 5000,
        'pause-on-hover' => TRUE,
      ],
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    // Slider settings.
    $image_styles = image_style_options(TRUE);

    $element['content'] = [
      '#title' => $this->t('Content display'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('content')['style'],
        $this->getSetting('content')['gutter'],
        $this->getSetting('content')['legend'],
      ],
    ];

    $element['content']['style'] = [
      '#title' => $this->t('Image style for content'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('content')['style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['content']['legend'] = [
      '#title' => $this->t('Display legend in overlay'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('content')['legend'],
    ];

    $element['content']['caption_background'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay background for legend'),
      '#empty_option' => $this->t('-None-'),
      '#options' => [
        'uk-overlay-default' => $this->t('default'),
        'uk-overlay-primary' => $this->t('Primary'),
        'uk-overlay-secondary' => $this->t('Secondary'),
      ],
      '#default_value' => $this->getSetting('content')['caption_background'],
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption transition'),
      '#empty_option' => $this->t('-None-'),
      '#default_value' => $this->getSetting('content')['caption_transition'],
      '#options' => [
        'uk-transition-fade' => $this->t('fade'),
        'uk-transition-slide-top' => $this->t('slide from the top'),
        'uk-transition-slide-bottom' => $this->t('slide from the bottom'),
        'uk-transition-slide-left' => $this->t('slide from the left'),
        'uk-transition-slide-right' => $this->t('slide from the right'),
        'uk-transition-slide-top-small' => $this->t('slide from the top with a smaller distance'),
        'uk-transition-slide-bottom-small' => $this->t('slide from the bottom with a smaller distance'),
        'uk-transition-slide-left-small' => $this->t('slide from the left with a smaller distance'),
        'uk-transition-slide-right-small' => $this->t('slide from the right with a smaller distance'),
        'uk-transition-slide-top-medium' => $this->t('slide from the top  with a medium distance'),
        'uk-transition-slide-bottom-medium' => $this->t('slide from the bottom  with a medium distance'),
        'uk-transition-slide-left-medium' => $this->t('slide from the left  with a medium distance'),
        'uk-transition-slide-right-medium' => $this->t('slide from the right  with a medium distance'),
      ],
      '#description' => $this->t('Transition effect for the caption'),
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption position'),
      '#default_value' => $this->getSetting('content')['caption_position'],
      '#options' => [
        'uk-position-top' => $this->t('Position element at the top'),
        'uk-position-top-left' => $this->t('Position element at the top left'),
        'uk-position-top-center' => $this->t('Position element at the top center'),
        'uk-position-top-right' => $this->t('Position element at the top right'),
        'uk-position-center' => $this->t('Position element vertically centered in the middle'),
        'uk-position-center-left' => $this->t('Position element vertically centered on the left'),
        'uk-position-center-right' => $this->t('Position element vertically centered on the right'),
        'uk-position-bottom' => $this->t('Position element at the bottom'),
        'uk-position-bottom-left' => $this->t('Position element at the bottom left'),
        'uk-position-bottom-center' => $this->t('Position element at the bottom center'),
        'uk-position-bottom-right' => $this->t('Position element at the bottom right'),
        'uk-position-left' => $this->t('Position element at the left'),
        'uk-position-right' => $this->t('Position element at the right'),
        'uk-position-cover' => $this->t('Position element to cover its container'),
      ],
      '#description' => $this->t('Position for the caption'),
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_modifier'] = [
      '#type' => 'select',
      '#title' => $this->t("Caption margin (modifier)"),
      '#empty_option' => $this->t('-None-'),
      '#default_value' => $this->getSetting('content')['caption_modifier'],
      '#options' => [
        'uk-position-small' => $this->t('Small'),
        'uk-position-medium' => $this->t('Medium'),
        'uk-position-large' => $this->t('Large'),
      ],
      '#description' => $this->t('Apply a margin to positioned caption'),
      '#fieldset' => 'slider_options',
    ];
    $element['content']['caption_toggle'] = [
      '#title' => $this->t('Display Legend only on hover'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('content')['caption_toggle'],
      '#fieldset' => 'slider_options',
    ];

    $element['content']['gutter'] = [
      '#title' => $this->t('Apply a gutter between slider items'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('content')['gutter'],
    ];

    $element['slider'] = [
      '#title' => $this->t('Slideshow settings'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('slider')['autoplay'],
        $this->getSetting('slider')['autoplay-interval'],
        $this->getSetting('slider')['center'],
        $this->getSetting('slider')['finite'],
        $this->getSetting('slider')['pause-on-hover'],
        $this->getSetting('slider')['sets'],
        $this->getSetting('slider')['velocity'],
      ],
    ];

    $element['slider']['autoplay'] = [
      '#title' => $this->t('slider autoplays'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slider')['autoplay'],
    ];

    $element['slider']['autoplay-interval'] = [
      '#title' => $this->t('The delay between switching slides in autoplay mode.'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('slider')['autoplay-interval'],
      '#min' => 3000,
      '#step' => 1000,
    ];

    $element['slider']['center'] = [
      '#title' => $this->t('Center the active slide'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slider')['center'],
    ];

    $element['slider']['finite'] = [
      '#title' => $this->t('Disable infinite sliding.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slider')['finite'],
    ];

    $element['slider']['pause-on-hover'] = [
      '#title' => $this->t('Pause autoplay mode on hover.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slider')['pause-on-hover'],
    ];

    $element['slider']['sets'] = [
      '#title' => $this->t('Slide in sets'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('slider')['sets'],
    ];

    $element['slider']['velocity'] = [
      '#title' => $this->t('The animation velocity (pixel/ms)'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('slider')['velocity'],
      '#min' => 1,
      '#step' => 1,
    ];

    // Number element.
    $element['nb-items'] = [
      '#title' => $this->t('Number element'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('nb-items')['width_'],
        $this->getSetting('nb-items')['width_@s'],
        $this->getSetting('nb-items')['width_@m'],
        $this->getSetting('nb-items')['width_@l'],
        $this->getSetting('nb-items')['width_@xl'],
      ],
    ];

    $args = [
      '@href' => 'https://getuikit.com/docs/grid',
      '@title' => 'Grid component - UIkit documentation',
    ];

    $breakpoints = [
      '' => $this->t('Affects all device widths'),
      '@s' => $this->t('Affects device widths of 640px and higher.'),
      '@m' => $this->t('Affects device widths of 960px and higher.'),
      '@l' => $this->t('Affects device widths of 1200px and higher.'),
      '@xl' => $this->t('Affects device widths of 1600px and higher.'),
    ];

    $element['nb-items']['grid_columns'] = [
      '#type' => 'item',
      '#title' => $this->t('Grid columns'),
      '#description' => $this->t("To create a grid whose child elements' widths are evenly split, we apply one class to the grid for each breakpoint. Each item in the grid is then automatically applied a width based on the number of columns selected for each breakpoint. See <a href='@href' target='_blank' title='@title'>Grid component</a> for more details.", $args),
    ];

    foreach (['', '@s', '@m', '@l', '@xl'] as $size) {
      $element['nb-items']["width_{$size}"] = [
        '#type' => 'select',
        '#title' => $this->t('uk-child-width-*@size', ['@size' => $size]),
        '#required' => FALSE,
        '#default_value' => $this->getSetting('nb-items')["width_{$size}"],
        '#options' => [
          "" => $this->t('Not Use'),
          "uk-child-width-1-1{$size}" => 1,
          "uk-child-width-1-2{$size}" => 2,
          "uk-child-width-1-3{$size}" => 3,
          "uk-child-width-1-4{$size}" => 4,
          "uk-child-width-1-5{$size}" => 5,
          "uk-child-width-1-6{$size}" => 6,
        ],
        '#description' => $breakpoints[$size],
      ];
    }

    // Navigation and Overlay.
    $element['navigation'] = [
      '#title' => $this->t('Navigation'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('navigation')['slidenav'],
        $this->getSetting('navigation')['dotnav'],
        $this->getSetting('navigation')['thumbnav'],
        $this->getSetting('navigation')['thumbnav-style'],
      ],
    ];

    $element['navigation']['slidenav'] = [
      '#title' => $this->t('Slidenav'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['slidenav'],
    ];

    $element['navigation']['slidenav_outside'] = [
      '#title' => $this->t('Display slidenav buttons outside slider'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['slidenav_outside'],
      '#fieldset' => 'slider_options',
    ];

    $element['navigation']['slidenav_big'] = [
      '#title' => $this->t('increase the size of the slidenav icons'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['slidenav_big'],
      '#fieldset' => 'slider_options',
    ];

    $element['navigation']['dotnav'] = [
      '#title' => $this->t('Dotnav'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['dotnav'],
    ];

    $element['navigation']['thumbnav'] = [
      '#title' => $this->t('Thumbnav'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['thumbnav'],
    ];

    $element['navigation']['thumbnav-style'] = [
      '#title' => $this->t('Image style for thumbnav'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('navigation')['thumbnav-style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['navigation']['light'] = [
      '#title' => $this->t('Inverse component'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('navigation')['light'],
      '#description' => $this->t('Add the .uk-light class to improve the visibility of objects on dark backgrounds in a light style.'),
    ];

    $element['lightbox'] = [
      '#title' => $this->t('Lightbox display'),
      '#type' => 'details',
      '#open' => TRUE,
      '#default_value' => [
        $this->getSetting('lightbox')['enabled'],
        $this->getSetting('lightbox')['style'],
        $this->getSetting('lightbox')['animation'],
        $this->getSetting('lightbox')['autoplay'],
        $this->getSetting('lightbox')['autoplay-interval'],
        $this->getSetting('lightbox')['pause-on-hover'],
      ],
    ];

    $element['lightbox']['enabled'] = [
      '#title' => $this->t('Enable lightbox'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('lightbox')['enabled'],
    ];

    $element['lightbox']['style'] = [
      '#title' => $this->t('Image style for content'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('lightbox')['style'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
    ];

    $animation_effects = [
      'slide' => $this->t('slide'),
      'fade' => $this->t('fade'),
      'scale' => $this->t('scale'),
    ];

    $element['lightbox']['animation'] = [
      '#title' => $this->t('Lightbox animation mode'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('lightbox')['animation'],
      '#options' => $animation_effects,
    ];

    $element['lightbox']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lightbox autoplays'),
      '#default_value' => $this->getSetting('lightbox')['autoplay'],
    ];

    $element['lightbox']['autoplay-interval'] = [
      '#type' => 'number',
      '#title' => $this->t('The delay between switching slides in autoplay mode.'),
      '#default_value' => $this->getSetting('lightbox')['autoplay-interval'],
      '#min' => 3000,
      '#step' => 1000,
    ];

    $element['lightbox']['pause-on-hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause autoplay mode on hover.'),
      '#default_value' => $this->getSetting('lightbox')['pause-on-hover'],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    // Implement settings summary.
    $summary = [];
    // Image style in slider.
    $image_styles = image_style_options(FALSE);
    if (isset($image_styles[$this->getSetting('content')['style']])) {
      $summary[] = $this->t('Content image style: @style', ['@style' => $image_styles[$this->getSetting('content')['style']]]);
    }
    else {
      $summary[] = $this->t('Content image style: Original image');
    }
    // Display legend in overlay.
    if ($this->getSetting('content')['legend']) {
      $summary[] = $this->t('Display legend in overlay: yes');
    }
    // Apply a gutter to the slider items.
    $summary[] = $this->t('Apply a gutter to the slider items: @gutter', ['@gutter' => $this->getSetting('content')['gutter'] ? 'yes' : 'no']);
    // Slider autoplay.
    $summary[] = $this->t('slider autoplays: @autoplay', ['@autoplay' => $this->getSetting('slider')['autoplay'] ? 'yes' : 'no']);
    // Slider autoplay interval.
    $summary[] = $this->t('Autoplay interval: @autoplay-interval (in milliseconds)', ['@autoplay-interval' => $this->getSetting('slider')['autoplay-interval']]);
    // Slider pause on hover.
    $summary[] = $this->t('Pause on hover: @pause-on-hover', ['@pause-on-hover' => $this->getSetting('slider')['pause-on-hover'] ? 'yes' : 'no']);
    // Slider center the list items.
    $summary[] = $this->t('Center the list items: @center', ['@center' => $this->getSetting('slider')['center'] ? 'yes' : 'no']);
    // Slider infinite loop.
    $summary[] = $this->t('Disable infinite sliding: @finite', ['@finite' => $this->getSetting('slider')['finite'] ? 'yes' : 'no']);
    // Slider slide by set.
    $summary[] = $this->t('Loop through a set of slides instead of single items: @sets', ['@sets' => $this->getSetting('slider')['sets'] ? 'yes' : 'no']);
    // Slider animation velocity.
    $summary[] = $this->t('The animation velocity (pixel/ms).: @velocity', ['@velocity' => $this->getSetting('slider')['velocity']]);
    // Slidenav.
    if ($this->getSetting('navigation')['slidenav']) {
      $summary[] = $this->t('Slidenav: yes');
    }
    // Dotnav.
    if ($this->getSetting('navigation')['dotnav']) {
      $summary[] = $this->t('Dotnav: yes');
    }
    // Thumbnav.
    if ($this->getSetting('navigation')['thumbnav']) {
      $summary[] = $this->t('Thumbnav: yes');
    }
    // Lightbox enabled.
    if ($this->getSetting('lightbox')['enabled']) {
      $summary[] = $this->t('Lightbox: yes');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    // Case field item is a media, we need to get the media source fields.
    if ($items->getItemDefinition()->getDataType() == "field_item:entity_reference") {
      $m_items = [];
      foreach ($items as $delta => $item) {
        // Magical.
        $media = $item->entity;
        $source_field_name = $media->getSource()->getConfiguration()['source_field'];
        $m_items[] = $media->get($source_field_name);
      }
      $items = $m_items;
    }

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'uikit_slider',
        '#item' => $item,
        '#display_settings' => $settings,
      ];
    }

    return $elements;
  }

}
